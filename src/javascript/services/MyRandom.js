// import change from 'change';

class MyRandom {

    static randomFloatNumber({
        min,
        max
    }) {
        return (Math.random() * (max - min) + min).toFixed(1);
    }
}
export default MyRandom;