import View from '../view'
class Modal extends View {
    modal;

    constructor(obj) {
        super();
        Modal.clearModals();

        this.modal = this.createElement({
            tagName: 'div',
            className: 'modal'
        });
        let close = this.createElement({
            tagName: 'div',
            className: 'close'
        });
        close.innerHTML = 'x';

        this.modal.append(close);
        close.addEventListener('click', () => {
            Modal.clearModals();
        });

        document.body.appendChild(this.modal);
    }


    static clearModals() {
        let modals = document.getElementsByClassName('modal');
        if (modals.length > 0)
            [...modals].forEach(elem => elem.remove());
    }
}
export default Modal;