import Modal from '../Modal';

class Text_modal extends Modal {
    // modal
    constructor(header, text) {
        super();
        let h = this.createElement({
            tagName: 'h2',
            className: 'text_modal_h2'
        });
        h.innerHTML = header;


        let div = this.createElement({
            tagName: 'div',
            className: 'text_modal_text'
        });
        div.innerHTML = text;

        let btn = this.createElement({
            tagName: 'button',
            classname: 'text_modal_button'
        });
        btn.innerHTML = 'OK';

        btn.addEventListener('click', () => {
            Modal.clearModals();
        });
        this.modal.append(h, div, btn);
    }


}
export default Text_modal;