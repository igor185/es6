import Modal from '../Modal'
import FighterView from '../../fighterView'

class Fighter_options_modal extends Modal {
    fighter;
    preparation;
    constructor(fighter, preparation) {
        super();
        Object.assign(this.fighter = {}, fighter);
        this.preparation = preparation;

        let h = this.createElement({
            tagName: 'h2'
        });
        h.innerHTML = this.fighter.name;
        this.modal.appendChild(h);

        let left = this.createElement({
            tagName: 'div',
            className: 'left'
        });
        left.append(new FighterView(fighter).element);

        let right = this.createElement({
            tagName: 'div',
            className: 'right'
        });
        right.append(this.createIndecator(this.fighter.attack, 'attack'),
            this.createIndecator(this.fighter.defense, 'defense'),
            this.createIndecator(this.fighter.health, 'health'));

        this.modal.append(left, right);

        this.modal.append(this.createBotton());
    }

    createIndecator(point, name) {

        let elem = this.createElement({
            tagName: 'div',
            className: 'indecator'
        });

        let block = this.createElement({
            tagName: 'div',
            className: 'inline'
        });
        block.innerHTML = name;

        let counter = this.createElement({
            tagName: 'span',
            className: 'counter'
        });
        counter.innerHTML = point;

        let minus = this.createElement({
            tagName: 'span',
            className: 'minus'
        });
        minus.innerHTML = '-';

        let plus = this.createElement({
            tagName: 'span',
            className: 'plus'
        });
        plus.innerHTML = '+';

        let countBlock = this.createElement({
            tagName: 'div',
            className: 'countBlock'
        });
        countBlock.append(minus, counter, plus)

        plus.addEventListener('click', () => {
            if (point == 0)
                minus.style.opacity = '1';
            this.fighter[name]++;
            point++;
            counter.innerHTML = point;
        });

        minus.addEventListener('click', () => {
            switch (point) {
                case 0:
                    break;
                case 1:
                    minus.style.opacity = '0.5';
                    this.fighter[name]--;
                    point--;
                    break;
                default:
                    this.fighter[name]--;
                    point--;
            }
            counter.innerHTML = point;
        });


        elem.append(block, countBlock);
        return elem;
    }

    createBotton() {
        let block = this.createElement({
            tagName: 'div',
            className: 'btnBlock'
        });

        let myBTN = this.createElement({
            tagName: 'button',
            className: 'btn'
        });
        myBTN.innerHTML = 'my fighter';
        myBTN.addEventListener('click', () => {
            this.preparation.your = this.fighter;
            Modal.clearModals();
        });

        let enemyBTN = this.createElement({
            tagName: 'button',
            className: 'btn'
        });

        enemyBTN.innerHTML = 'enemy fighter';
        enemyBTN.addEventListener('click', () => {
            this.preparation.enemy = this.fighter;
            Modal.clearModals();
        });

        block.append(myBTN, enemyBTN);
        return block;
    }

}
export default Fighter_options_modal;