import FightersView from './fightersView';
import {
  fighterService
} from './services/fightersService';
import Fighter from './Fighter';


class App {
  yourFigher;

  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');
  static information = document.getElementById('informationBlock')

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';

      let fighters = await fighterService.getFighters();
      fighters = [...fighters].map(elem => new Fighter(elem));

      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;

      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
      App.information.style.visibility = 'visible';
    }
  }
}

export default App;