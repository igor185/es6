class Indecator {
    fighter;
    elem;
    indecator;
    _health;
    span;

    constructor(fighter) {
        this.fighter = fighter;
        this._health = fighter.health;

        this.elem = document.createElement('div');
        this.elem.setAttribute('class', 'progress');
        this.indecator = document.createElement('div');
        this.indecator.setAttribute('class', 'indecatorHealth');
        this.span = document.createElement('span');
        this.span.innerHTML = this.fighter.health;

        this.elem.append(this.span, this.indecator);
    }

    set health(point) {
        console.log(point)
        this._health = (this._health.toFixed(1) - point);
        if(this._health <= 0){
            
        }
        console.log(this._health)
        this.span.innerHTML = this._health.toFixed(1);
        let width = (100 * (this._health).toFixed(1) / this.fighter.health).toFixed(1);
        console.log(width)
        this.indecator.style.width = `${width}%`;
    }
}
export default Indecator;