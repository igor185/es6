import FighterView from '../fighterView';
import Text_modal from '../helpers/modal/Text_modal';
import Indecator from './Indecator';

class Battle {
    you;
    enemy;
    root;
    text;
    firstIndecator;
    secondIndecator;

    constructor(you, enemy) {
        document.body.innerHTML = '';
        this.enemy = enemy;
        this.you = you;
        this.init();
        // this.showInformation();

        let generator = this.fight({
            you: this.you,
            enemy: this.enemy
        });

        let btn = document.createElement('button');
        btn.innerHTML = 'Press';
        btn.addEventListener('click', () => {
            this.hit(generator.next().value);
        });

        this.root.append(btn);
        this.log('The figth start!');

    }
    hit({
        attack,
        defense,
        count,
        hit = +(attack - defense).toFixed(1)
    }) {
        let print = '';
        console.log(attack, defense, hit);
        if (count % 2 == 1) {
            print += (`Your attack: ${attack.toFixed(1)}, Enemy defense: ${defense.toFixed(1)}. `);
            if (hit > 0) {
                print += (`You did ${(hit).toFixed(1)} damage`)
                this.secondIndecator.health = (hit).toFixed(1);
            } else
                print += (`You did 0 damage`);
        } else {
            print += (`Enemy attack: ${attack.toFixed(1)}, Your defense: ${defense.toFixed(1)}. `);
            if (hit > 0) {
                print += (`Enemy did ${(hit).toFixed(1)} damage`);
                this.firstIndecator.health = (hit).toFixed(1);
            } else
                print += (`Enemy did 0 damage`);
        }
        this.log(print);
    }

    init() {
        this.root = document.createElement('div')
        this.root.setAttribute('id', 'rootElem');

        let header = document.createElement('div');
        header.setAttribute('class', 'header');

        this.firstIndecator = new Indecator(this.you);
        header.append(this.firstIndecator.elem);

        let span = document.createElement('span')
        span.setAttribute('class', 'headerSpan');
        span.innerHTML = 'VS';
        header.append(span);

        this.secondIndecator = new Indecator(this.enemy);
        header.append(this.secondIndecator.elem);
        this.root.append(header);

        let fighters = document.createElement('div')
        fighters.setAttribute('class', 'fighters');
        fighters.append(new FighterView(this.you).element);
        fighters.append(new FighterView(this.enemy).element);
        this.root.append(fighters)



        this.text = document.createElement('textarea');
        this.text.setAttribute('id', 'textarea');
        this.text.setAttribute('disabled', 'disabled');

        this.root.append(this.text);

        document.body.append(this.root);
    }
    log(...text) {

        text.forEach(elem => {
            this.text.value += `${elem} \n`;
            // console.log(elem)
            // if (typeof elem != 'object')
            //     this.text.value += `${elem} \n`;
            // else
            //     Object.keys(elem).forEach(elem2 => this.text.value += `${elem[elem2]} \n`);
        });
    }
    showInformation() {

            document.body.append(new Text_modal('There ara information about fight', `You will have modal window, where you need to choose where you want to attack
        All information during fight will be in console`).modal);
        }
        * fight() {
            let count = 2;
            while (true) {
                if (count++ % 2 == 0) {
                    yield {
                        attack: this.you.getHitPower(),
                        defense: this.enemy.getBlockPower(),
                        count
                    };
                } else
                    yield {
                        attack: this.enemy.getHitPower(),
                        defense: this.you.getBlockPower(),
                        count
                    };
            }

        }
}

export default Battle;