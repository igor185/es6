let express = require('express');
let path = require('path');
let app = express();

app.use(express.static(path.join(__dirname)));

app.use((req, res) => {
    res.sendFile('index.html');
});

let port = process.env.PORT;

app.listen(port = 5050, function () {
    console.log('My Application Running  on http://localhost:' + port + '/');
});